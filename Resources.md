# Some useful resources to master ggplot2

* The ggplot2 Cheat Sheet is taken from https://github.com/rstudio/cheatsheets/blob/master/data-visualization-2.1.pdf

* A ggplot2 tutorial from the book **R for Data Science ** by Garrett Grolemund and Hadley Wickham : https://r4ds.had.co.nz/data-visualisation.html

* Introduction to ggplot2 : https://stats.idre.ucla.edu/r/seminars/ggplot2_intro/

* Another introduction to ggplot2: https://www.slideshare.net/izahn/rgraphics-12040991

* The official (and good) book by Hadley Wickham : https://www.amazon.com/dp/0387981403/ref=cm_sw_su_dp?tag=ggplot2-20 


