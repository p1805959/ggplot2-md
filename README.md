This directory contains the data used for the practical about MD data analysis.

At the end of the practical, you should commit: 
- A Figure to explain the movements between the monomers,
- A legend to explain the Figure,
- The code used to generate the Figure.
