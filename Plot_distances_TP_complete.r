library(ggplot2)
D_intra=read.table("Distances_intra.txt")
D_inter=read.table("Distances_inter.txt")

D_intra1=D_intra[D_intra$run==1,]

# Mouvements internes de chaque monomère
# Distance 1_3  en fonction du temps, sous forme de points 
ggplot(D_intra1,aes(Time,d1_3))+geom_point()
# On voit bien qu'il y a deux séries de points, qui correspondent au deux monomères.


# Colorons d'après les monomères 
ggplot(D_intra1,aes(Time,d1_3,col=monomer))+geom_point()


# Ce serait mieux en ligne non ?
ggplot(D_intra1,aes(Time,d1_3,col=monomer))+geom_point()+geom_line()
ggplot(D_intra1,aes(Time,d1_3,col=monomer))+geom_line()

# Par contre si on n'avait pas spécifier la couleur, il n'aurait pas pu grouper les séries de points:
ggplot(D_intra1,aes(Time,d1_3,linetype=monomer))+geom_line()

# Donc il faut rajouter un groupe :
ggplot(D_intra1,aes(Time,d1_3,group=monomer))+geom_line()

# Avec de la couleur :
ggplot(D_intra1,aes(Time,d1_3,group=monomer,col=monomer))+geom_line()


# Un exemple de faceting, si on veut regarder les 4 réplicats :
ggplot(D_intra,aes(Time,d1_3,group=monomer,col=monomer))+geom_line()+facet_wrap(~run)+ggtitle("Distance 1 3")

# Avec les valeurs initiales :
# avec une ligne horizontale
ggplot(D_intra,aes(x=Time,y=d1_3,col=monomer))+geom_line()+facet_wrap(~run)+geom_hline(data=D_intra[D_intra$Time==0.0,], aes( yintercept=d1_3,col=monomer),linetype="dashed")

# avec un point
ggplot(D_intra,aes(x=Time,y=d1_3,col=monomer))+geom_line()+facet_wrap(~run)+geom_point(data=D_intra[D_intra$Time==0.0,], aes(Time,d1_3),col="red")


# Faire pareil avec la distance 1_6 et 3_6:


# leur montrer comment on fait un plot avec toutes les distances sur le même plot
ggplot(D_intra,aes(Time,d1_3))+geom_line(col="orange")+facet_wrap(~run*monomer,ncol=4)+
geom_line(aes(Time,d1_6),col="blue")+geom_line(aes(Time,d3_6),col="grey")+ylim(0,30)+ylab("intra-monomer helix distance")


# Et en reformattant le jeu de données 
library(reshape)
D_intra_melt=melt(D_intra,id=c("Time","monomer","run"),variable_name="dist")


ggplot(D_intra_melt,aes(Time,value,col=dist))+geom_line()+facet_wrap(~run*monomer,ncol=4)






# Et comment on peut passer en densité assez facilement 

# Avec le jeu de données initial 
ggplot(D_intra,aes(d1_3))+facet_wrap(~run*monomer,ncol=4)+geom_density(col="orange")+geom_density(aes(d1_6),col="blue")+geom_density(aes(d3_6),col="grey")


# Avec le jeu de données reformatté : 
ggplot(D_intra_melt,aes(value,col=dist))+geom_density()+facet_wrap(~run*monomer,ncol=4)
# en rajoutant la valeur initiale 
ggplot(data=D_intra_melt,aes(value,fill=dist,col=dist))+geom_density(alpha=0.5)+facet_grid(run~monomer)+geom_vline(data=D_intra_melt[D_intra_melt$Time==0,],aes(xintercept=value,col=dist))


# Est-ce que les mouvments sont concertés ??

# Analyse de la paire de distances d1_3,d1_6
ggplot(D_intra1,aes(d1_3,d1_6))+geom_point()+facet_wrap(~monomer)

# Avec de la transparence 
ggplot(D_intra1,aes(d1_3,d1_6))+geom_point(alpha=0.2)+facet_wrap(~monomer)

# Avec des distributions marginales 
ggplot(D_intra1,aes(d1_3,d1_6))+geom_point(alpha=0.2)+facet_wrap(~monomer)+geom_rug(alpha=0.03)

# Avec de la transparence et de la couleur 
ggplot(D_intra1,aes(d1_3,d1_6,col=monomer))+geom_point(alpha=0.2)+geom_rug(alpha=0.03)


# Avec de la transparence et de la couleur et le point de départ initial 
ggplot(D_intra1,aes(d1_3,d1_6,col=monomer))+geom_point(alpha=0.2)+geom_point(data=D_intra1[D_intra1$Time==0.0,],shape=21,colour="black", aes(fill=monomer))

ggplot(D_intra1,aes(d1_3,d1_6,col=monomer))+geom_point(alpha=0.2)+geom_point(data=D_intra1[D_intra1$Time==0.0,],shape=21,colour="black", aes(fill=monomer))+geom_density2d()


# Avec une coloration en fonction du temps de simulation 
ggplot(D_intra1,aes(d1_3,d1_6,col=Time))+geom_point()+geom_point(data=D_intra[D_intra$Time==0.0,],colour="red", aes(fill=monomer))+facet_wrap(~monomer)

# En changeant la couleur 
ggplot(D_intra1,aes(d1_3,d1_6,col=Time))+geom_point()+geom_point(data=D_intra[D_intra$Time==0.0,],shape=21,colour="black", aes(fill=monomer))+facet_wrap(~monomer)+scale_colour_continuous(low="yellow",high="red")


# idem, avec un chemin :
ggplot(D_intra1,aes(d1_3,d1_6,col=Time,fill=Time))+geom_path()+geom_point(data=D_intra[D_intra$Time==0.0,],shape=22,colour="black")+facet_wrap(~monomer)+scale_colour_gradient(low="yellow",high="red")+scale_fill_gradient(low="yellow",high="red")



# on peut facilement faire des opérations sur les colonnes directement dans ggplot2 
ggplot(D_intra,aes(Time,d1_3+d1_6,col=monomer))+facet_wrap(~run)+geom_line()




## Fin du TP accompagné.
# regarder en autonomie la conformation du pore avec les distances intra et inter monomères 










